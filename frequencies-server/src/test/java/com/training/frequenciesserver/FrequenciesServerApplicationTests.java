package com.training.frequenciesserver;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class FrequenciesServerApplicationTests {

	private static final int RESPONSE_SIZE_IN_BYTES = 32;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnResponseWithStatus200() throws Exception {
		ResponseEntity<byte[]> response = getResponse(byte[].class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnByteDataOfExpectedLength() throws Exception {
		ResponseEntity<byte[]> response = getResponse(byte[].class);

		assertEquals(RESPONSE_SIZE_IN_BYTES, response.getBody().length);
	}

	@Test
	void whenGettingTwoFrequencies_shouldReturnDifferentData() throws Exception {
		byte[] frequency1 = getResponse(byte[].class).getBody();
		byte[] frequency2 = getResponse(byte[].class).getBody();

		boolean isFrequeciesEqual = Arrays.equals(frequency1, frequency2);

		assertFalse(isFrequeciesEqual, "Got 2 equal frequencies: " + Arrays.toString(frequency1));
	}

	@Test
	void whenGetToFrequencyEndpoint_shouldReturnResponseWithContentTypeOctecStream() throws Exception {
		ResponseEntity<String> response = getResponse(String.class);

		MediaType expectedContentType = response.getHeaders().getContentType();

		assertEquals(MediaType.APPLICATION_OCTET_STREAM, expectedContentType);
	}

	private <T> ResponseEntity<T> getResponse(Class<T> bodyClass) {
		return this.restTemplate.getForEntity("http://localhost:" + port + "/frequency", bodyClass);
	}

}
